# frozen_string_literal: true

module Services
  # This service is only used in development mode to get the files from the
  # "public" folder so that we don't have to upload it to S3 every time.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Local
    # @!attribute [r] public_folder
    #   @return [String] the public folder to eliminate from URLs
    attr_reader :public_folder

    def initialize(public_folder)
      @public_folder = public_folder
    end

    def js
      files('js')
    end

    def css
      files('css')
    end

    def files(extension)
      Dir[File.join(public_folder, extension, '**', "*.#{extension}")].map do |file|
        file.gsub(public_folder, '')
      end
    end
  end
end
