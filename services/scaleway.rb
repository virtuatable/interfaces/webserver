# frozen_string_literal: true

require 'singleton'

module Services
  # This service lists the files needed in the static view of the site.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Scaleway
    include Singleton

    # @!attribute [r] pool
    #   @return [Hash<String, Array<String>>] the hash making the correspondance between
    #     the type of file, and the linked list of files.
    attr_reader :pool
    # @!attribute [r] build
    #   @return [String] the identifier of the build to display.
    attr_reader :build
    # @!attribute [r] bucket
    #   @return [String] the name of the bucket to get the files from.
    attr_reader :bucket
    # @!attribute [r] s3
    #   @return [Aws::S3::Resource] the S3 connection to the platform
    attr_reader :s3
    # @!attribute [r] public_url
    #   @return [String] the base URL for all S3 files
    attr_reader :public_url

    def initialize
      @pool = {}
      @s3 = create_s3_connection
      @build = ENV['BUILD_NUMBER']
      @bucket = ENV['S3_BUCKET'].strip
      @public_url = ENV['S3_ENDPOINT'].gsub('https://', "https://#{@bucket}.")
    end

    def js
      files 'js'
    end

    def css
      files 'css'
    end

    private

    def files(extension)
      prefix = "#{build}/#{extension}"
      # Serves as a "cache" mecanism returning the same result without querying
      # again if another user demands the same content. Resets after restart.
      unless pool.key? extension
        pool[extension] = s3.client.list_objects(bucket: bucket, prefix: prefix).contents.map do |c|
          File.join(public_url, c.key)
        end
      end

      pool[extension]
    end

    def create_s3_connection
      Aws::S3::Resource.new(
        endpoint: ENV['S3_ENDPOINT'].strip,
        access_key_id: ENV['S3_ACCESS_KEY'].strip,
        secret_access_key: ENV['S3_SECRET_KEY'].strip,
        region: ENV['S3_REGION'].strip,
        force_path_style: true
      )
    end
  end
end
