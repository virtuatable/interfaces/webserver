# frozen_string_literal: true

module Controllers
  # Main controller of the application, serving the JS app, and forwarding
  # calls to the API by securely adding the application key.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  class Frontend < Sinatra::Base
    configure do
      set :root, File.absolute_path(File.join(__dir__, '..'))
      set :public_folder, File.join(settings.root, 'public')
      set :views, File.join(settings.root, 'views')

      use Rack::Session::Cookie, secret: ENV['SESSION_SECRET']
      use Rack::Csrf, raise: true
    end

    # Route to render the main template file and the Javascript application
    get '/*' do
      erb :'index.html'
    end

    helpers do
      def csrf_tag
        Rack::Csrf.tag(env)
      end

      def service
        return Services::Local.new(settings.public_folder) if ENV['RACK_ENV'] == 'development'

        Services::Scaleway.instance
      end
    end
  end
end
