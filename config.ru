# frozen_string_literal: true

require 'bundler'
Bundler.require :runtime

require './services/scaleway'
require './services/local'
require './controllers/frontend'

run Controllers::Frontend.new
