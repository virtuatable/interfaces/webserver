#!/usr/bin/env bash

function web {
	echo "
	+------------------------+
	| Starting the Webserver |
	+------------------------+
	"
	bundle exec rackup -p 80 -o 0.0.0.0 --env production
}

function bash {
	echo "
	+------------------------+
	| Starting a login shell |
	+------------------------+
	"
	/bin/bash --login
}

case "$1" in
	"web")
	web
	;;

	"bash")
	bash
	;;

	*)
	echo "
	+--------------------------------+
	| Usage: docker_command [ARG]    |
	| [ARG]:                         |
	| - 'web' to start the webserver |
	| - 'bash' to run a login shell  |
	+--------------------------------+
	"
	exit 0
	;;
esac